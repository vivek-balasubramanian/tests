import saga
import os
import sys
WORKDIR = '/N/u/vivek91/tryout/'
USERNAME = 'vivek91'
if __name__ == '__main__':

	#Identity
	ctx = saga.Context("ssh")
	ctx.user_id = "%s"%(USERNAME)
	session = saga.Session()
	session.add_context(ctx)


	js = saga.job.Service("ssh://%s@sierra.futuregrid.org"%(USERNAME), session=session)
	jd = saga.job.Description()	
	jd.working_directory = WORKDIR
	jd.executable      = 'python'
	jd.arguments       = [WORKDIR+'/testL1.py']
	jd.output          = "stdout_l1.txt"
        jd.error           = "stderr_l1.txt"
	workdir = saga.filesystem.Directory("sftp://%s@sierra.futuregrid.org%s"%(USERNAME,WORKDIR),saga.filesystem.CREATE_PARENTS)
	
	#Transfer file to be run in L1 and L2
	mbpy = saga.filesystem.File("file://localhost%s/testL1.py"%os.getcwd())
    	mbpy.copy(workdir.get_url())
    	mbpy = saga.filesystem.File("file://localhost%s/testL2.py"%os.getcwd())
    	mbpy.copy(workdir.get_url())
    	

    	myjob = js.create_job(jd)
	print "Job State : %s" % (myjob.state)
        myjob.run()
	print "Job State : %s" % (myjob.state)
        myjob.wait()

       	print "Job State : %s" % (myjob.state)
       	print "Exitcode  : %s" % (myjob.exit_code)
  	
  	print os.getcwd()
  	
  	#Bring back outputs created at L1(created at L1) and L2(created at L2)
  	for txt in workdir.list('L1.txt'):
  		print 'found L1'
	  	workdir.copy('L1.txt','sftp://localhost/%s/L1.txt'%os.getcwd())
	for txt in workdir.list('L2.txt'):
  		print 'found L2'
	  	workdir.copy('L2.txt','sftp://localhost/%s/L2.txt'%os.getcwd())
