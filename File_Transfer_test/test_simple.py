import saga
import os
WORKDIR = '/N/u/vivek91/tryout/'
if __name__ == '__main__':

    #Identity
    ctx = saga.Context("ssh")
    ctx.user_id = "vivek91"
    session = saga.Session()
    session.add_context(ctx)


    js = saga.job.Service("ssh://vivek91@hotel.futuregrid.org", session=session)
    jd = saga.job.Description()
    #jd.environment     = {'MYOUTPUT':'"Hello from SAGA"'}
    #jd.working_directory = WORKDIR
    jd.executable      = 'python'
    jd.arguments       = [WORKDIR+'/testL2.py']
    jd.output          = "stdout_l2.txt"
    jd.error           = "stderr_l2.txt"
    workdir = saga.filesystem.Directory("sftp://vivek91@hotel.futuregrid.org/N/u/vivek91/tryout",saga.filesystem.CREATE_PARENTS)
    mbpy = saga.filesystem.File("file://localhost/%s/testL2.py"%os.getcwd())
    mbpy.copy(workdir.get_url())

    myjob = js.create_job(jd)
    myjob.run()
    myjob.wait()
    print "Job Stat  : %s" % (myjob.state)
    print "Exitcode  : %s" % (myjob.exit_code)

    #for txt in workdir.list('L2.txt'):
    #print 'found L2', txt,  workdir.list('L2.txt')
    workdir.copy('L2.txt','sftp://localhost/%s/'%os.getcwd())
