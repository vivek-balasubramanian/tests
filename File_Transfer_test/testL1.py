import saga
import os
import sys
USERNAME = 'vivek91'
WORKDIR = '/N/u/%s/tryout/'%USERNAME
if __name__ == '__main__':

	#Identity
	ctx = saga.Context("ssh")
	ctx.user_id = "%s"%(USERNAME)
	session = saga.Session()
	session.add_context(ctx)


	js = saga.job.Service("ssh://%s@hotel.futuregrid.org"%(USERNAME), session=session)
	jd = saga.job.Description()	
	jd.working_directory = WORKDIR
	jd.executable      = 'python'
	jd.arguments       = [WORKDIR+'/testL2.py']
	jd.output          = "stdout_l2.txt"
        jd.error           = "stderr_l2.txt"
	workdir = saga.filesystem.Directory("sftp://%s@hotel.futuregrid.org%s"%(USERNAME,WORKDIR),saga.filesystem.CREATE_PARENTS)

	#Transfer file to be run in L2
    	mbpy = saga.filesystem.File("file://localhost%s/testL2.py"%os.getcwd())
    	mbpy.copy(workdir.get_url())
    	
    	myjob = js.create_job(jd)
        print "Job State : %s" % (myjob.state)
        myjob.run()
	print "Job State : %s" % (myjob.state)
        myjob.wait()

       	print "Job State : %s" % (myjob.state)
       	print "Exitcode  : %s" % (myjob.exit_code)
  	
  	#Create a file in L1
  	f1 = open('L1.txt','w')
	f1.write('This is in sierra - L1')
	f1.close()
	
	#Copy file back from L2 to L1
  	for txt in workdir.list('L2.txt'):
  		print 'found L2'
  		workdir.copy('L2.txt','sftp://localhost%s/L2.txt'%(WORKDIR))
